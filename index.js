const Discord = require("discord.js");
const bot = new Discord.Client();

var botSettings = require("./BotSettings.json");
var fs = require('fs');

const cooks = [
	"Your mom lol.",
	"Your mom so fat she broke the stairway to heaven.",
	"No u",
	"You have and impeccable grasp of the obvious",
	"Your so weak that a noodle could snap you.",
	"An ant has a higher IQ then you.",
	"Bacteria are more cultured then you",
	"Your the random hair in my eye." 
];

bot.on("ready", async () => {
	console.log('['+(new Date()).toLocaleString([], {hour:'2-digit', minute:'2-digit',second:'2-digit',hour12: false})+'] <console> '+bot.user.username + " powering up.")
	try {
	let link = await bot.generateInvite(["ADMINISTRATOR"]);
	console.log(link);
	console.log('['+(new Date()).toLocaleString([], {hour:'2-digit', minute:'2-digit',second:'2-digit',hour12: false})+'] <console> '+"I am ready.");
	}catch(e){
		console.log(e.stack);
	}
});

bot.on("message", async message => {
	if(message.author.bot) return;
	if(message.channel.type === "dm"){
		message.channel.send("lol fuck off");
		return;
	}

	let messageArray = message.content.split(" ");
	let comman = messageArray[0];
	let args = messageArray.slice(1);

	if(!comman.startsWith(botSettings.prefix)) return;

	if(comman.startsWith(botSettings.prefix)){
		var command = comman.slice(botSettings.prefix.length);
		switch(command){
			case "help":
				let helpEmbed = new Discord.RichEmbed()
					.setColor("#00FF00")
					.addField(botSettings.prefix +"help", "Display this.")
					.addField(botSettings.prefix +"userinfo", "Displays info about you.")
					.addField(botSettings.prefix +"hello , hey, hi", "Bot says hi.")
					.addField(botSettings.prefix +"cook", "Cooks you emotionally.")
					.addField(botSettings.prefix +"ping", "Pong [ping]")
					.addField(botSettings.prefix +"setpre", "Set the bot prefix")

				message.channel.send(helpEmbed);
			break;
			
			case "userinfo":
				var embed = new Discord.RichEmbed()
					.setAuthor(message.author.username)
					.addField("Full Username", message.author.tag)
					.addField("Discord ID", message.author.id)
					.addField("Discord Birthday" , message.author.createdAt)
					.setColor("#ff33cc")

				message.channel.send(embed);
			break;
			
			case "hello":
			case "hey":
			case "hi":
				message.channel.send("Hey there.");
			break;

			case "birthday":
				message.channel.send("Your discord birthday is: " + message.author.createdAt);
			break;
			
			case "cook":
				var rand = Math.floor(Math.random()*5);
				message.channel.send(cooks[rand]);
			break;	

			case "ping":
				var ping = bot.pings[0];
				message.channel.send(ping);
			break;

			case "setprefix":
				botSettings.prefix = args[0];
				console.log(botSettings.prefix);
				fs.writeFile('./BotSettings.json', JSON.stringify(botSettings, null, 2), function (err){ 
					if(err){ console.log(err)}
					console.log("writing to json");
				})
			break;
			
			case "kick":
				if(!message.guild.member(message.author).hasPermission("KICK_MEMBERS")) return message.channel.send("Ya dont have perms.");
				if(!message.guild.member(bot.user).hasPermission("KICK_MEMBERS")) return message.channel.send("I dont have perms.");
				var user = message.mentions.users.first();
				var reason = args.slice(1).join(" ");

				if(message.mentions.users.size != 1) return message.channel.send("Mention one user pls");
				if(!reason) return message.channel.send("Gotta give a reason my guy");

				message.guild.member(user).kick();

				
				var log = bot.channels.find("name", "log");

				log.send(`Kicked ${user.tag} \nReason: ${reason}`);
			break;	

			case "ban":
				if(!message.guild.member(message.author).hasPermission("BAN_MEMBERS")) return message.channel.send("Ya dont have perms.");
				if(!message.guild.member(bot.user).hasPermission("BAN_MEMBERS")) return message.channel.send("I dont have perms.");
				var user = message.mentions.users.first();
				var reason = args.slice(1).join(" ");

				if(message.mentions.users.size != 1) return message.channel.send("Mention one user pls");
				if(!reason) return message.channel.send("Gotta give a reason my guy");

				message.guild.member(user).ban(reason);

				var log = bot.channels.find("name", "log");

				log.send(`:b:anned ${user.tag} \nReason: ${reason}`);	
			break;
			
			case "addemote":

			break;

			default:
				message.channel.send("Thats not a command you absolute ***d i n g u s***");
		}	
	}	
});
bot.on("message", async message => {
	console.log('['+(new Date()).toLocaleString([], {hour:'2-digit', minute:'2-digit',second:'2-digit',hour12: false})+'] '+'<'+message.author.tag+' '+'#'+message.channel.name+'>'+' '+message.content);
});

bot.login(botSettings.token);
